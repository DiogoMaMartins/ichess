import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import initialiseChessBoard from '../../utils/initialiseChessBoard'
import BoardGame from '../../components/BoardGame';
import Square from '../../components/Square';
import ContainerMoves from '../../components/ContainerMoves';
import Background from '../../assets/background/4.jpg';

function Game(){
	const [squares,setSquares] = useState(initialiseChessBoard);
	const [player,setPlayer] = useState(1);
	const [sourceSelection,setSourceSelection] = useState(-1);
	const [status,setStatus] = useState('');
	const [turn,setTurn] = useState('white');
	const [whiteFallenSoldiers,setWhiteFallenSoldiers] = useState([] as any);
	const [blackFallenSoldiers,setBlackFallenSoldiers] = useState([] as any);



	const renderSquare = (square:any, i:any) => {
    return <Square 
    piece = {square} 
    style = {square.style}
    />
  }


	const isMoveLegal = (srcToDestPath:any) => {
    let isLegal = true;
    for(let i = 0; i < srcToDestPath.length; i++){
      if(squares[srcToDestPath[i]] !== null){
        isLegal = false;
      }
    }
    return isLegal;
  }

	const handleClick = (i:number) => {
		const square = squares.slice();
    
    if(sourceSelection === -1){
    	console.log("sourceSelection")
      if(!square[i] || square[i].player !== player){
      	setStatus("Other player to play")
      	//if i click in a square where don't have pieces

        //this.setState({status: "Wrong selection. Choose player " + player + " pieces."});
        console.log(square[i])
        return square[i] ? delete square[i].style.backgroundColor : null;
      } else{
      /*	squares[i].style={
      		...square[i].style, backgroundColor:"RGB(111,143,114)"
      	};*/
      	// when whe pick a piece 
      	console.log('i',i)
      	setStatus("Choose destination for the selected piece")
      	setSourceSelection(i)
      }
	} else if (sourceSelection > -1) {
		// delete squares[sourceSelection].style.backgroundColor;
		//if we already selected a square

		if(square[i] && square[i].player === player){
			setStatus("Wrong selection Choose valid source and destination again")
			setSourceSelection(-1)
		} else {
			const square = squares.slice();
			const whiteFallenSoldiersS = whiteFallenSoldiers.slice();
        const blackFallenSoldiersS = blackFallenSoldiers.slice();
			const isDestEnemyOccupied = square[i]? true : false;
			const isMovePossible = squares[sourceSelection].isPossibleToMove(sourceSelection, i, isDestEnemyOccupied);
        	const srcToDestPath = square[sourceSelection].getSrcToDestPath(sourceSelection, i);
        	const isMoveLegalS = isMoveLegal(srcToDestPath);

        	if(isMovePossible && isMoveLegalS){
        		if(square[i] !== null){
        			console.log('im square',square[i])
        			if(square[i].player === 1){
        				 whiteFallenSoldiersS.push(square[i]);
        			} else {
        				 blackFallenSoldiersS.push(square[i]);
        			}
        		}

        		 square[i] = square[sourceSelection];
         		 square[sourceSelection] = null;
		         let playerS = player === 1? 2: 1;
		         let turnState = turn === 'white'? 'black' : 'white';

		         setSourceSelection(-1)
		         setSquares(square)
		         setWhiteFallenSoldiers(whiteFallenSoldiersS)
		         setBlackFallenSoldiers(blackFallenSoldiersS)
		         setPlayer(playerS)
		         setStatus('')
		         setTurn(turnState)


        	} else {
        		setStatus("Wrong selection Choose valid source and destination again")
				setSourceSelection(-1)
        	}
		}
	}

}

	return(
		<div style={{backgroundImage:"url('"+Background+"')",backgroundSize:'cover',height:'100%',position:'absolute',top:0,left:0,right:0,bottom:0}}>
			<h1>Ichess</h1>
			<h3>{status}</h3>
			<Grid container direction="row" alignItems="center" justify="space-around" >
						
				
						<BoardGame squares = {squares}
           				 onClick = {(i:number) => handleClick(i)}/>

           				 <Grid item xs={3} md={4} lg={4}>
           				 	
           					<ContainerMoves/>

           					 <div>
							      <div className="board-row">{whiteFallenSoldiers.map((ws:any, index:number) =>
							       renderSquare(ws, index)
							        )}</div>
							      <div className="board-row">{blackFallenSoldiers.map((bs:any, index:number) =>
							        renderSquare(bs, index)
							        )}</div>
						      </div>
           				 </Grid>
         
			</Grid>
		</div>
		)
}

export default Game;