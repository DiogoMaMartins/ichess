import React from 'react';
import { Paper } from '@material-ui/core';
import useStyles from './styles';
import Square from '../Square';



function BoardGame(props:any){
	const classes = useStyles();

	const renderSquare = (index:number,squareShade:string) => {
			return <Square
				key={index}
				piece = {props.squares[index]}
				style = { props.squares[index] ? props.squares[index].style : null}
				shade = {squareShade}
				onClick = { () => props.onClick(index)}
			/>
	}

	const isEven = (n:number) => {
		return n % 2 === 0
	}

	const board = [];

	for(let i = 0; i < 8; i++){
		const squareRows = [];

		for(let j = 0; j < 8; j++){
			const squareShade = (isEven(i) && isEven(j)) || (!isEven(i) && !isEven(j))? "lightSquare" : "darkSquare"; 
			 squareRows.push(renderSquare((i*8) + j, squareShade));
		}

		 board.push(<div  key={i} className="board-row">{squareRows}</div>)

	}

	return(
			<Paper   elevation={0} className={classes.root}>
			{ board }
			</Paper>
		)
}


export default BoardGame;