import { makeStyles } from '@material-ui/core/styles';

export default  makeStyles(theme => ({
 darkSquare:{
   backgroundColor:'#b58863',
   border:'1px solid transparent',
   float:'left',
   fontSize:"24px",
   fontWeight:'bold',
   lineHeight:'34px',
   height:'80px',
   marginRight:'-1px',
   marginTop:'-1px',
   padding:0,
   texAlign:'center',
   width:'80px',
   "&:focus": {
      outline:'none',
    },
 },
 lightSquare:{
   backgroundColor:'#f0d9b5',
   border:'1px solid transparent',
   float:'left',
   fontSize:"24px",
   fontWeight:'bold',
   lineHeight:'34px',
   height:'80px',
   marginRight:'-1px',
   marginTop:'-1px',
   padding:0,
   texAlign:'center',
   width:'80px',
   "&:focus": {
      outline:'none',
    },
 }
}));
