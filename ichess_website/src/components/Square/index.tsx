import React from 'react';
import useStyles from './styles';

export default function Square(props:any) {
	const classes = useStyles();

    return (
    	<>
    {
    	props.shade === 'darkSquare' ? 
    		<button className={classes.darkSquare}

		      onClick={props.onClick}
		      style={props.style}>
		      </button>

		      :

		      <button  className={classes.lightSquare}

		      onClick={props.onClick}
		      style={props.style}></button>
    }
    </>
     
    );
  
}