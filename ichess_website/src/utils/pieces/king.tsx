import Piece from './piece';
import DarkKing from '../../assets/pieces/dark/king.svg';
import LightKing from '../../assets/pieces/light/king.svg';


export default class King extends Piece {
	constructor(player:number){
		super(player, (player === 1 ? LightKing : DarkKing))
	}

	 isPossibleToMove(src:any,dest:number){
		return (src - 9 === dest ||
			  src - 8 === dest || 
		      src - 7 === dest || 
		      src + 1 === dest || 
		      src + 9 === dest || 
		      src + 8 === dest || 
		      src + 7 === dest || 
		      src - 1 === dest);
			
	}

	/**
   * always returns empty array because of one step
   * @return {[]}
   */
  getSrcToDestPath(src:any, dest:number){
    return [];
  }
}