import Piece from './piece';
import DarkRook from '../../assets/pieces/dark/rook.svg';
import LightRook from '../../assets/pieces/light/rook.svg';


export default class Rook extends Piece {
	constructor(player:number){
		super(player, (player === 1 ? LightRook : DarkRook))
	}

	 isPossibleToMove(src:any,dest:number){
		let mod = src % 8;
    	let diff = 8 - mod;
    	return (Math.abs(src - dest) % 8 === 0 || (dest >= (src - mod) && dest < (src + diff)));
  	}

	/**
   * always returns empty array because of one step
   * @return {[]}
   */
  getSrcToDestPath(src:any, dest:number){
   let path = [], pathStart, pathEnd, incrementBy;
    if(src > dest){
      pathStart = dest;
      pathEnd = src;
    }
    else{
      pathStart = src;
      pathEnd = dest;
    }
    if(Math.abs(src - dest) % 8 === 0){
      incrementBy = 8;
      pathStart += 8;
    }
    else{
      incrementBy = 1;
      pathStart += 1;
    }

    for(let i = pathStart; i < pathEnd; i+=incrementBy){
      path.push(i);
    }
    return path;
  }
}