import Piece from './piece';
import DarkBishop from '../../assets/pieces/dark/bishop.svg';
import LightBishop from '../../assets/pieces/light/bishop.svg';


export default class Bishop extends Piece {
	constructor(player:number){
		super(player, (player === 1 ? LightBishop : DarkBishop))
	}

	 isPossibleToMove(src:number,dest:number){
		return (Math.abs(src - dest) % 9 === 0 || Math.abs(src - dest) % 7 === 0);

			
	}

	/**
   * always returns empty array
   * @return {[]}
   */
  getSrcToDestPath(src:number, dest:number){
    let path = [], pathStart, pathEnd, incrementBy;
    if(src > dest){
      pathStart = dest;
      pathEnd = src;
    }
    else{
      pathStart = src;
      pathEnd = dest;
    }
    if(Math.abs(src - dest) % 9 === 0){
      incrementBy = 9;
      pathStart += 9;
    }
    else{
      incrementBy = 7;
      pathStart += 7;
    }

    for(let i = pathStart; i < pathEnd; i+=incrementBy){
      path.push(i);
    }
    return path;
  }
}