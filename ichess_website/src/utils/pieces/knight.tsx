import Piece from './piece';
import DarkKnight from '../../assets/pieces/dark/knight.svg';
import LightKnight from '../../assets/pieces/light/knight.svg';


export default class Knight extends Piece {
	constructor(player:number){
		super(player, (player === 1 ? LightKnight : DarkKnight))
	}

	isPossibleToMove(src:number, dest:number){
    	return (src - 17 === dest || 
	      src - 10 === dest || 
	      src + 6 === dest || 
	      src + 15 === dest || 
	      src - 15 === dest || 
	      src - 6 === dest || 
	      src + 10 === dest || 
	      src + 17 === dest);
	  }


	/**
   * always returns empty array 
   * @return {[]}
   */
  getSrcToDestPath(src:number, dest:number){
    return [];
  }
}