 export default class Piece {
  style: { backgroundImage: string,backgroundRepeat:string,backgroundPosition:string,backgroundSize:string;  }

  constructor(public player: number, pieceImage: any){
    this.style = {backgroundImage: "url('"+pieceImage+"')", backgroundRepeat:"no-repeat",backgroundPosition:"center",backgroundSize:"contain"};
  }
}